import threading
import lightsFlatcolor
import lightsOff
import lightsStrobe
import lightsPong
import lightsRainbow
import lightsChristmas

from rpi_ws281x import Color

from flask import Flask
app = Flask(__name__)

lightThread = lightsOff.OffThread()

@app.route('/flatcolor/<r>/<g>/<b>')
def flatColorHandler(r,g,b):
    global lightThread

    killThread()

    lightThread = lightsFlatcolor.FlatColorThread(Color(int(r),int(g),int(b)))
    lightThread.start()
    return 'Success'

@app.route('/off')
def off():
    global lightThread

    killThread()

    lightThread = lightsOff.OffThread()
    lightThread.start()
    return 'Success'

@app.route('/strobe')
def strobe():
    global lightThread

    killThread()

    lightThread = lightsStrobe.StrobeThread()
    lightThread.start()
    return 'Success'

@app.route('/pong/<n_balls>')
def pong(n_balls):
    global lightThread

    n_balls = int(n_balls)

    killThread()

    lightThread = lightsPong.PongThread(n_balls)
    lightThread.start()
    return 'Success'

@app.route('/rainbow/<speed>')
def rainbowHandler(speed):
    global lightThread

    killThread()

    lightThread = lightsRainbow.RainbowThread(int(speed))
    lightThread.start()
    return 'Success'

@app.route('/christmas')
def christmas():
    global lightThread

    killThread()

    lightThread = lightsChristmas.ChristmasThread()
    lightThread.start()
    return 'Success'

def killThread():
    if lightThread.started:
        lightThread.stop()
        lightThread.join()
