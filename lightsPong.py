from lightConfig import *
from ball import Ball

import threading

class PongThread(threading.Thread):
    def __init__(self, n_balls):
        self.running = False
        self.started = False
        self.balls = []
        self.n_balls = n_balls
        super().__init__()

    def start(self):
        self.started = True
        self.running = True
        self.initBalls(self.n_balls)
        super().start()

    def run(self):
        self.pong()
        self.running = False

    def stop(self):
        self.running = False

    def pong(self):
        while self.running:
            self.iterFrame()

    def iterFrame(self):
        for ball in self.balls:
            ball.clear()
        for ball in self.balls:
            ball.advance()
        for ball in self.balls:
            for other_ball in self.balls:
                if other_ball != ball:
                    ball.checkCollisions(other_ball)
        for ball in self.balls:
            ball.draw()
        strip.show()
        time.sleep(0.01)

    def initBalls(self, n):
        standard_width = int(strip.numPixels()/3/n)
        for i in range(n):
            self.balls.append(Ball(strip, width=standard_width, starting_pos=int(i*1.5*standard_width)))
