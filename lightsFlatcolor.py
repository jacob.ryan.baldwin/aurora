from lightConfig import *
import threading

class FlatColorThread(threading.Thread):
    def __init__(self, color):
        self.running = False
        self.started = False
        self.color = color
        super(FlatColorThread, self).__init__()

    def flatColor(self, strip, color):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, color)
        strip.show()

    def start(self):
        self.started = True
        self.running = True
        super(FlatColorThread, self).start()

    def run(self):
        self.flatColor(strip, self.color)
        self.running = False

    def stop(self):
        self.running = False
