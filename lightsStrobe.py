from lightConfig import *
import threading

class StrobeThread(threading.Thread):
    def __init__(self):
        self.running = False
        self.started = False
        super().__init__()

    def strobe(self, strip):
        while self.running:
            self.flatColor(strip, Color(255,255,255))
            time.sleep(0.01)
            self.flatColor(strip, Color(0,0,0))
            time.sleep(0.01)

    def flatColor(self, strip, color):
        for i in range(strip.numPixels()):
            strip.setPixelColor(strip.numPixels() - i, color)
        strip.show()

    def start(self):
        self.started = True
        self.running = True
        super().start()

    def run(self):
        self.strobe(strip)
        self.running = False

    def stop(self):
        self.running = False
