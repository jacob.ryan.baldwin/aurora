from lightConfig import *

class Ball:
    def __init__(self, strip, starting_pos=0, color=Color(255,255,255), speed=1, direction=1, width=1):
        self.color = color
        self.pos = starting_pos
        self.speed = speed
        self.direction = direction
        self.width = width

    def clear(self):
        for i in range(self.width):
            strip.setPixelColor(self.pos + i, Color(0, 0, 0))

    def withinBounds(self, pos):
        if pos >= self.pos and pos <= self.pos + self.width:
            return True
        else:
            return False

    def checkCollisions(self, other_ball):
        if self.direction == 1 and other_ball.withinBounds(self.pos + self.width):
            self.randomizeColor()
            if other_ball.direction == self.direction:
                other_ball.speed += 1
            self.direction = -1
            self.randomlyChangeSpeed()

        elif self.direction == -1 and other_ball.withinBounds(self.pos):
            self.randomizeColor()
            if other_ball.direction == self.direction:
                other_ball.speed += 1
            self.direction = 1
            self.randomlyChangeSpeed()

    def advance(self):
        if self.pos >= strip.numPixels() - self.width:
            self.direction = -1
        if self.pos <= 0:
            self.direction = 1
        self.pos = self.pos + self.speed * self.direction

    def draw(self):
        for i in range(self.width):
            strip.setPixelColor(self.pos + i, self.color)

    def randomlyChangeSpeed(self):
        self.speed = self.speed + random.randint(-1,1)
        if self.speed == 0:
            self.randomlyChangeSpeed()

        if self.speed > self.width/4:
            self.speed = self.speed - 2

    def randomizeColor(self):
        self.color = Color(random.randint(0,255), random.randint(0,255), random.randint(0,255))
