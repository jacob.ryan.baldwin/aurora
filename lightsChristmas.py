from lightConfig import *
import threading

class ChristmasThread(threading.Thread):
    def __init__(self):
        self.running = False
        self.started = False
        super(ChristmasThread, self).__init__()

    def christmas(self, strip):
        segment_length = 6
        offset = 0
        while self.running:
            for i in range(0, strip.numPixels()):
                if (i+offset)%segment_length in (1, 2):
                    strip.setPixelColor(i, Color(0, 255, 0))
                elif (i+offset)%segment_length in (4, 5):
                    strip.setPixelColor(i, Color(255, 0, 0))
                else:
                    strip.setPixelColor(i, Color(255, 255, 255))

            offset = (offset + 1) % segment_length

            time.sleep(.1)

            strip.show()

    def start(self):
        self.started = True
        self.running = True
        super(ChristmasThread, self).start()

    def run(self):
        self.christmas(strip)
        self.running = False

    def stop(self):
        self.running = False
