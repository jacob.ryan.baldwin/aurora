from lightConfig import *
import threading
from rpi_ws281x import Color

class RainbowThread(threading.Thread):
    def __init__(self, speed):
        self.running = False
        self.started = False
        self.speed = speed
        super(RainbowThread, self).__init__()

    def rainbowCycle(self, wait_ms=20, iterations=1):
      """Draw rainbow that uniformly distributes itself across all pixels."""
      while self.running:
        for j in range(256*iterations):
          for i in range(strip.numPixels()):
              strip.setPixelColor(i, self.wheel((int(i * 256 / strip.numPixels()) + j) & 255))
          strip.show()
          time.sleep(wait_ms/1000.0)

          # check if self.running is now false to keep rainbow from taking forever to quit
          if self.running == False:
              return

    def wheel(self, pos):
      """Generate rainbow colors across 0-255 positions."""
      if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
      elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
      else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)

    def start(self):
        self.started = True
        self.running = True
        super(RainbowThread, self).start()

    def run(self):
        self.rainbowCycle(wait_ms=100-self.speed)
        self.running = False

    def stop(self):
        self.running = False
