from lightConfig import *
import threading

class OffThread(threading.Thread):
    def __init__(self):
        self.running = False
        self.started = False
        super().__init__()

    def flatColor(self, strip, color):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, color)
        strip.show()

    def start(self):
        self.started = True
        self.running = True
        super().start()

    def run(self):
        self.flatColor(strip, Color(0,0,0))
        self.running = False

    def stop(self):
        self.running = False
